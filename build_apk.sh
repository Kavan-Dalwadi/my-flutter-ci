export MY_PATH=$PWD
cd ~/Android/bin/
export PATH=$PATH:$PWD
cd ~
export ANDROID_HOME=$PWD/Android/
#yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses
#sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-30" >/dev/null
#sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;30.0.3" >/dev/null
cd $MY_PATH
flutter doctor
flutter clean
flutter pub get
flutter build apk --release --no-sound-null-safety
flutter build web
cd build/app/outputs/flutter-apk/
ls
aws s3 cp ./app-release.apk s3://my-flutter-ci/flutter_builds/
